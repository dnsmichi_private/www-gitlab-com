---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2021-01-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 146       | 11.47%          |
| Based in EMEA                               | 349       | 27.42%          |
| Based in LATAM                              | 20        | 1.57%           |
| Based in NORAM                              | 758       | 59.54%          |
| **Total Team Members**                      | **1,273** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 871       | 68.42%          |
| Women                                       | 402       | 31.58%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,273** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 153       | 68.30%          |
| Women in Management                         | 71        | 31.70%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **224**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 73        | 72.28%          |
| Women in Leadership                         | 28        | 27.72%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **101**   | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 435       | 79.52%          |
| Women in Engineering                        | 112       | 20.48%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **547**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 51        | 7.20%           |
| Black or African American                   | 17        | 2.40%           |
| Hispanic or Latino                          | 35        | 4.94%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 29        | 4.10%           |
| White                                       | 436       | 61.58%          |
| Unreported                                  | 139       | 19.63%          |
| **Total Team Members**                      | **708**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 8.91%           |
| Black or African American                   | 3         | 1.49%           |
| Hispanic or Latino                          | 5         | 2.48%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 3.96%           |
| White                                       | 135       | 66.83%          |
| Unreported                                  | 33        | 16.34%          |
| **Total Team Members**                      | **202**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 6.52%           |
| Black or African American                   | 2         | 1.45%           |
| Hispanic or Latino                          | 4         | 2.90%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 6         | 4.35%           |
| White                                       | 85        | 61.59%          |
| Unreported                                  | 32        | 23.19%          |
| **Total Team Members**                      | **138**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 11        | 13.75%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.75%           |
| White                                       | 50        | 62.50%          |
| Unreported                                  | 16        | 20.00%          |
| **Total Team Members**                      | **80**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 130       | 10.21%          |
| Black or African American                   | 30        | 2.36%           |
| Hispanic or Latino                          | 65        | 5.11%           |
| Native Hawaiian or Other Pacific Islander   | 2         | 0.16%           |
| Two or More Races                           | 37        | 2.91%           |
| White                                       | 710       | 55.77%          |
| Unreported                                  | 298       | 23.41%          |
| **Total Team Members**                      | **1,273** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 63        | 11.52%          |
| Black or African American                   | 10        | 1.83%           |
| Hispanic or Latino                          | 25        | 4.57%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 14        | 2.56%           |
| White                                       | 306       | 55.94%          |
| Unreported                                  | 128       | 23.40%          |
| **Total Team Members**                      | **547**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 16        | 7.14%           |
| Black or African American                   | 2         | 0.89%           |
| Hispanic or Latino                          | 6         | 2.68%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.13%           |
| White                                       | 138       | 61.61%          |
| Unreported                                  | 55        | 24.55%          |
| **Total Team Members**                      | **224**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 12        | 11.88%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 0.99%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 2.97%           |
| White                                       | 60        | 59.41%          |
| Unreported                                  | 25        | 24.75%          |
| **Total Team Members**                      | **101**   | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.18%           |
| 25-29                                       | 202       | 15.87%          |
| 30-34                                       | 361       | 28.36%          |
| 35-39                                       | 290       | 22.78%          |
| 40-49                                       | 277       | 21.76%          |
| 50-59                                       | 109       | 8.56%           |
| 60+                                         | 19        | 1.49%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,273** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
